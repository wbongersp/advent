<?php

$data = file_get_contents('input.txt');
$rules = explode("\n",$data);


class Bag {
    var $name = '';
    var $contents = [];

    function __construct()
    {

    }

    function buildFromRule($rule) {
        if(preg_match('/^([a-z]+ [a-z]+) bags contain (.*)$/', $rule,$matches)) {
            $this->name = $matches[1];
            $this->buildContents($matches[2]);
        }
    }

    function buildContents($contentsRule) {
        if(preg_match_all('/([0-9]+) ([a-z]+ [a-z]+)/',$contentsRule,$matches)) {
            foreach ($matches[1] as $key=>$amount)
                $this->contents[] = new BagContents($amount,$matches[2][$key]);
        }
    }

    function setBagContentsObjects($bags) {
        foreach ($this->contents AS $content) {
            $content->setObject($bags);
        }
    }

    function canContainCollerction($name, &$collection) {

        return $collection;
    }

    function getContentsCount($indent = '') {
        $count = 0;
        if(empty($this->contents)) {
            return $count;
        }
        echo $indent . '1 ' . $this->name . ' contains:' . PHP_EOL;
        $indent .= '  ';
        foreach ($this->contents as $content) {
            echo $indent . $content->amount . ' ' . $content->name . PHP_EOL;
            $count += $content->amount * (1 + $content->object->getContentsCount($indent));
        }
        return $count;
    }

    function canContain($name)
    {
        foreach ($this->contents as $content) {
            if ($content->name === $name || $content->object->canContain($name)) {
                return TRUE;
            }
        }
        return FALSE;
    }

}

class BagContents {
    function __construct($amount,$name)
    {
        $this->amount = $amount;
        $this->name = $name;
    }

    function setObject($bags) {
        foreach ($bags as $bag) {
            if($bag->name === $this->name) {
                $this->object = $bag;
                return;
            }
        }
    }

}

$bags = [];
foreach ($rules AS $rule) {
    $bag = new Bag();
    $bag->buildFromRule($rule, $bags);
    $bags[] = $bag;
}
foreach ($bags as $bag) {
    $bag->setBagContentsObjects($bags);
}

$count = 0;
/** @var Bag $bag */
$collection = [];
foreach ($bags as $bag) {
    if($bag->canContain('shiny gold')) {
        $collection[$bag->name] = $bag->name;
    }
//    $bag->canContainCollerction('shiny gold',$collection);
}

echo 'A1: ' . count($collection);

echo PHP_EOL;
$goldBag = null;
foreach ($bags as $bag) {
    if($bag->name === 'shiny gold') {
        $goldBag = $bag;
    }
}

echo 'A2: ' . $goldBag->getContentsCount();

//print_r($bags);