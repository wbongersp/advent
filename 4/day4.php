<?php

$data = file_get_contents('day4.txt');

$entries = preg_split('/\n[\s]*\n/',$data);
$valid = 0;

function dateValid($value,$min,$max) {
    return (preg_match('/[0-9]{4}/',$value) && $value >= $min && $value <= $max);
}


$test = 'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm';

//byr (Birth Year) - four digits; at least 1920 and at most 2002.
//iyr (Issue Year) - four digits; at least 2010 and at most 2020.
//eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
//hgt (Height) - a number followed by either cm or in:
//If cm, the number must be at least 150 and at most 193.
//If in, the number must be at least 59 and at most 76.
//hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
//ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
//pid (Passport ID) - a nine-digit number, including leading zeroes.
//cid (Country ID) - ignored, missing or not.

$validatiors = [
//    'byr:(19[2-9][0-9]|200[0-2])',
    'byr:1973',
    'eyr:2020',
    'pid:860033327'
];

echo 'Testing:' . PHP_EOL;
$expression = '/((' . implode(')|(',$validatiors) . ')){1}/';
echo 'Expr: '  . $expression . PHP_EOL;
if(preg_match($expression,$test)) {
    echo 'OK';
}
exit;




foreach ($entries AS $entry) {
    preg_match_all('/([a-z]+):(.*?)[\s\n]/i',' ' . $entry . ' ',$matches);
    if(!empty($matches[1])) {
        $this_valid = FALSE;

        switch (count($matches[1])) {
            case 8:
                $this_valid = TRUE;
                break;
            case 7:
                if(!in_array('cid',$matches[1])) {
                    $this_valid = TRUE;
                }
                break;
            default:
                break;
        }

        if($this_valid) {
            $errors = 0;
            foreach($matches[1] as $key=>$category) {
                $value = $matches[2][$key];
                echo $category . ' -> ' . $value . PHP_EOL;

                switch ($category) {

                    case 'byr':
                        $errors += dateValid($value,1920,2020) ? 1:0;
                        break;
                    case 'iyr':
                        $errors += dateValid($value,2010,2020) ? 1:0;
                        break;
                    case 'eyr':
                        $errors += dateValid($value,2020,2030) ? 1:0;
                        break;
                    case 'hgt':
                        if(preg_match('/([0-9])([a-z]{2})/i',$value,$height_matches)) {
                          print_r($height_matches);
                          switch ($height_matches[2]) {
                              case 'cm':
                                  $errors += ($height_matches[1] >= 150 && $height_matches[1] <= 193);
                                  break;
                              case 'in':
                                  $errors += ($height_matches[1] >= 59 && $height_matches[1] <= 76);
                                  break;
                              default:
                                  $errors++;

                          }
                        } else {
                            $errors++;
                        }

                }


            }

        }
    }
}
echo $valid;
