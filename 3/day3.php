<?php

class Vector {
    var $x = 0;
    var $y = 0;
    var $map = [];

    function move($x,$y) {
        $this->x += $x;
        $this->y += $y;
    }

    function loadMap($map) {
        $this->map = $map;
    }

    function done() {
        return empty($this->map[$this->y]);
    }

    function getLocation() {
        $line = $this->map[$this->y];
        $x = $this->x % strlen($line);
        return substr($line,$x,1);
    }

    function onTree() {
        return $this->getLocation() === '#';
    }

    function reset() {
        $this->x = 0;
        $this->y = 0;
    }
}
$map = preg_split("/\n/",file_get_contents('day3.txt'));

$position = new Vector();
$position->loadMap($map);

$slopes = [[1,1],[3,1],[5,1],[7,1],[1,2]];
$product = 1;
foreach ($slopes as $movement) {
    $trees = 0;
    while(!$position->done()) {
        $trees += ($position->onTree()) ? 1 : 0;
        $position->move($movement[0],$movement[1]);
    }
    echo '(' . $movement[0] . ', ' . $movement[1] . ') ' . $trees . PHP_EOL;
    $product *= ($trees > 0) ? $trees : 1;
    $position->reset();
}

echo 'Total: ' . $product . PHP_EOL;


// Alternate solution
$slopes = [[1,1],[3,1],[5,1],[7,1],[1,2]];
$product = 1;
foreach ($slopes as $movement) {
    $trees = 0;
    foreach ($map as $lineNum => $line) {
        if(($lineNum % $movement[1]) !== 0) {
            continue;
        }
        $x = ($movement[0] * $lineNum/$movement[1]) % strlen($line);
        $trees += (substr($line,$x,1) === '#') ? 1:0;
    }

    echo '(' . $movement[0] . ', ' . $movement[1] . ') ' . $trees . PHP_EOL;
    $product *= ($trees > 0) ? $trees : 1;
}

echo 'Total: ' . $product . PHP_EOL;


