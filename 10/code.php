<?php

$data = file_get_contents('input.txt');

$testData = '16
10
15
5
1
11
7
19
6
12
4';




$testData2 = '28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3';

AdapterSequence::solve($testData);
AdapterSequence::solve($testData2);
AdapterSequence::solve($data);

exit();

class AdapterSequence {

    var $code;
    var $series = [];
    var $variationCount = 0;

    function __construct($series)
    {
        $this->code = self::getDiffCode($series);
    }

    function addSeries($series) {
        if(self::getDiffCode($series) !== $this->code) {
            return FALSE;
        }
        $this->series[] = $series;
    }

    function getVariationCount() {

        $baseSeries = $this->series[0];

        // Get all variations for the base series, but chop off the first and last element.
        $variations = [];
        $this->getArrayVariations(array_slice($baseSeries,1,-1),$variations);

        // Put first and last element back on everything in the series
        $variations = array_map(function($series) use ($baseSeries) {
            return reset($baseSeries) . '-' . $series . '-' . $baseSeries[count($baseSeries)-1];
        },$variations);

        // Clean the keys
        $variations = array_values($variations);

        // Add completely empty series
        $variations[] = reset($baseSeries) . '-' . $baseSeries[count($baseSeries)-1];

        // Filter out invalid combinations
        $validSeries = array_filter($variations,function($seriesString) {
            return AdapterSequence::isValidSeries(explode('-',$seriesString));
        });
        return count($validSeries) ;
    }

    /**
     * Get all variations of an array by sequentially unsetting keys.
     * The unique series are set as strings as keys and values
     *
     * @param $base
     * @param $variations
     */
    function getArrayVariations($base, &$variations) {
        $base = array_values($base);
        if(!AdapterSequence::isValidSeries($base)) {
            return;
        }

        $variations[implode('-',$base)] = implode('-',$base);
        if(count($base) > 0) {
            for ($i = 0; $i < count($base); $i++) {
                $less = $base;
                unset($less[$i]);
                if(!empty($less)) {
                    $this->getArrayVariations($less,$variations);
                }
            }
        }
    }

    /**
     * Return true if a series is valid.
     * A series is valid if there is no difference of more than 3 between two numbers.
     *
     *
     * @param array $series collection of numbers
     * @return bool
     */
     static function isValidSeries($series) {
        foreach ($series as $key=>$number) {
            if($key === 0) {
                continue;
            }
            if( $number - $series[$key-1] > 3 ) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * Get the multiplier to represent how many variations are possible based on the codes in this sequence.
     *
     * @return float|int
     */
    function getMultiplier() {
        return pow($this->getVariationCount(),count($this->series));
    }

    /**
     * Find and output the variation count
     *
     * @param string $data Raw data
     */
    static function solve($data) {

        $data = explode("\n",$data);
        $data[] = 0;
        $data[] = max($data) + 3;
        sort($data);

        $variableSeries = self::getVariableBlocks($data);
        $sequences=[];
        foreach($variableSeries as $series) {
            $code = AdapterSequence::getDiffCode($series);
            if(!isset($sequences[$code])) {
                $sequences[$code] = new AdapterSequence($series);
            }
            $sequences[$code]->addSeries($series);
        }

        $multiplier = 1;
        foreach ($sequences as $sequence) {
            $multiplier *= $sequence->getMultiplier();
        }
        echo 'Possibilities: ' . $multiplier . PHP_EOL;

    }

    static function getDiffCode($series) {
        $diffCode = '';
        foreach ($series as $key=>$number) {
            if($key > 0) {
                $diffCode .= $number - $series[$key-1];
            }
        }
        return $diffCode;
    }

    /**
     * Go through the data (array of adapter sizes) and find a list of series numbers we can take out.
     * Every list will always have the first and last number we cannot take out but need to know for further validation.
     * So this series
     * [0,1,4,23] will return [0,1,4] (1 can be taken out
     * [0,1,2,4,19,20,21] will return [0,1,2,4] and [19,20,21]
     *
     * @param $data
     * @return array|null
     */
    static function getVariableBlocks($data) {
        $series = null;
        $allSeries = [];

        // Go through the data
        foreach ($data as $key=>$number) {
            if($key === 0 || $key === count($data) -1) {
                continue;
            }
            $prev = $data[$key-1];
            $next = $data[$key+1];

            if($next - $prev > 3) {
                if($series !== null) {
                    $series[] = $number;
//                $series[] = $next;
                    $allSeries[] = $series;
                    $series = null;
                }
                continue;
            }

//        echo $prev . ' - ' . $number . ' - ' . $next . PHP_EOL;

            if($series === null) {
                $series = [$prev];
            }
            $series[] = $number;


//        echo $next-$prev .  '--' . $prev .' = ' . $number . ' = ' . $next . ' ' . PHP_EOL;
        }


        if($series !== null) {
            echo 'LEFT:';
            print_r($series);
            exit();
        }

        return $allSeries;
    }
}


//
//foreach ($inputs as $data) {
//    $diffs = [3=>1];
//    $last = 0;
//    sort($data);
//
//    foreach ($data as $num) {
//        if(!isset($diffs[$num-$last])) {
//            $diffs[$num-$last] = 0;
//        }
//        $diffs[$num-$last]++;
//        $last = $num;
//    }
//}


/* So here's the rules
  We have an array of random numbers in ascending, being either 1,2 or 3 apart.
 How many combinations can we make while the array still conforms to the rules of
 - being max 3 apart
 - first number minimum 3
 - you cannot remove the last number, ie second last number min lastNum -3
 So:
  [1,2,5] is valid
  [3,4,5] is valid
  [4.5] is invalid
  [3,6,10] is invalid

   In an array where we can only remove 1 number this is easy, there are 2 combinations
  [3,4,5] and [3,5]

   If we can remove two numbers there 4 options
   [3,4,5,8,9,10] [3,5,8,9,10] [3,4,5,6,10] and [3,5,6,10]
   2 + 1 + 1
   If we could remove 3 numbers it would be
   4 + 2 + 1 + 1

   It becomes more complex if we can remove one number but it invalidates removing the other
   [3,4,5,7,10] we can remove both 4 and 5 but if we remove them together it creates [3,7,10] which is invalid
   so we only have 3 options
   2 + 1 + 1 (-1 invalid option)
   If we add an option to the above we get twice the options (6)

   Let's translate it to bits to make it easier to understand (0=off):
   000, 001, 010, 011, 100, 101, 110, 111 (8 options if we could turn everything off
   But we know that all these bits are invalid
   000,001 invalidating 2 of our options


   Now what if we have 2 invalid options
   [3,4,5,7,10,11,12,14]: 4,5,11 and 12 can be removed but 4 and 5 not together and 11 and 12 not together
   In binary
   00-- is invalid and  --00 is invalid

   So instead of 2^4 combinations we have to subsstract 2^2 * 2 combinations 16 - 8 = 8.
   This is incomplete since we countend 0000 twice, so the correct number is 9 combinations

   There is another complication,consider this:
   [3,4,5,6,7,10], we could remove 4,5 and 6. We can even remove 4 and 5 [3,6,7] together and 5 and 6 [3,4,7] together
   but not all three together [3,7]
   So only 000 is not allowed 7 options

   [3,4,5,6,7,8,10], we could remove 4,5,6 and 7. Not valid are
   We can disable any three numbers as long as they are not next to eachother
   So invalid are
   000- and -000 again the overlap to deal with, if we write it out:

0000 *
0001 *
0010
0011
0100
1000 *
1001
1010
1011
1100
1101
1111

    Instead of the expected 4 situations we should only substract 3 because we know that 0000 appears in both cases.

    Can we always substract just 1 less or is it more complicated?
    What is we have --000, -000- and 000--, obviously 00000 would still overlap them all, but also 10000 (first 2) and 00001 (second 2)
    Leading to 3*2^2 - 3 invalid states.

000-- 2 new invalid states
-000- 1 new invalid states
--000 0 new invalid states



    ==========New approach ======
    Say we have this
    [1,2,3,4,7,8,9,12]
    we can leave out 2,3 and 8
    So we know 1,4,7,12 will be in every sequence. The variation will be in 2 blocks
    [1,2,3,4] and [7,8,9]
    We can define if in patterns of numbers based on the deffences
    111 and 11

    First has 2 variations, other has 2




*/
//
//
//foreach ($inputs as $data) {
//    // Collect numbers you can take out
//    $data[] = 0;
//    $data[] = max($data) + 3;
//    sort($data);
//
//    print_r($data);
////    continue;
////
////    $variations = [];
////    getArrayVariations($data,$variations);
////    echo  'Variations count ' . count($variations);
////    continue;
//
//
//    // Start by adding the first and last input
////    array_unshift($data,0);
////    $data = [0] + $data;
//
//    $allSeries = [];
//
//
////    sort($data);
//
//    echo 'Adapaters: ' . PHP_EOL;
//    echo implode(' - ',$data) . PHP_EOL;
//
//
//    $blocks = [];
//
//    $allSeries = getVariableBlocks($data);
//
////
////    print_r($allSeries);
////    continue;
//
//    // Create ann array with grouped blocks of similar
//    $diffSeries=[];
//    foreach ($allSeries as $series) {
//        // Generate diff code by combining the differences of numbers into a string
//        $diffCode = '';
//        foreach ($series as $key=>$number) {
//            if($key > 0) {
//                $diffCode .= $number - $series[$key-1];
//            }
//        }
//        if(!isset($diffSeries[$diffCode])) {
//            $diffSeries[$diffCode] = [
//                'variations' => getPossiblitiesCount($diffCode),
//                'items' => [],
//            ];
//        }
//        $diffSeries[$diffCode]['items'][] = $series;
//    }
//
//    $possibilities = 1;
//    foreach ($diffSeries as $diffString=>$info) {
//        echo 'COUNT ('.$diffString.'): ' . $info['variations'] . ' ^ ' . count($info['items']) . PHP_EOL;
//        $possibilities *= pow($info['variations'], count($info['items']));
//
//    }
//    echo 'Possiblities: ' . $possibilities . PHP_EOL;
//
////    print_r($allSeries);
////    print_r($diffSeries);
//
//}
//
//
//
//
