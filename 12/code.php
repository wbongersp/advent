<?php

$data = file_get_contents('input.txt');

$testData = 'F10
N3
F7
R90
F11';

$ship = new Ship($testDatadata);
$ship->go();
echo 'A1 test ' . $ship->sumPosition() . PHP_EOL;

$ship = new Ship($data);
$ship->go();
echo 'A1 ' . $ship->sumPosition() . PHP_EOL;


$ship = new ShipWithWayPoint($testData);
$ship->go();
echo 'A2 test ' . $ship->sumPosition() . PHP_EOL;

$ship = new ShipWithWayPoint($data);
$ship->go();
echo 'A2 ' . $ship->sumPosition() . PHP_EOL;




/**
 * Class Ship
 *
 *
 */
class Ship {
    var $direction = 'E';
    var $instructions = [];
    var $position;

    public function __construct($data)
    {
        $this->position = new Position(0,0);
        $list = explode("\n",$data);
        foreach ($list as $item) {
            $this->instructions[] = new Instruction($item,$this);
        }
    }

    public function go() {
        foreach ($this->instructions AS $instruction) {
            $instruction->go();
            echo $this->outputPosition() . PHP_EOL;
        }
    }

    public function move($amount)
    {
        $this->moveDirection($this->direction, $amount);
    }

    public function moveDirection($direction, $amount) {
        $this->position->moveDirection($direction,$amount);
    }

    public function rotate($amount) {
        $directions = ['N','E','S','W'];
        $currentKey = array_search($this->direction,$directions);

        $newKey = $currentKey + $amount/90;
        while($newKey < 0) {
            $newKey += 4;
        }
        while($newKey > 3) {
            $newKey -= 4;
        }
        $this->direction = $directions[$newKey];
    }

    public function outputPosition() {
        return $this->position->output();

    }

    public function sumPosition() {
        return $this->position->sum();
    }
}

class ShipWithWayPoint extends Ship {
    var $wayPoint;

    function __construct($data)
    {
        $this->wayPoint = new WayPoint(10,1);
        parent::__construct($data);
    }


    public function move($amount)
    {
        $this->position->moveToPosition($this->wayPoint->position,$amount);
//        $this->moveDirection($this->direction, $amount);
    }

    public function moveDirection($direction, $amount) {
        $this->wayPoint->moveDirection($direction, $amount);
//        $this->position->moveDirection($direction,$amount);
    }


    public function outputPosition() {
        $output = parent::outputPosition() . PHP_EOL;
        $output .= '--' . $this->wayPoint->position->output();
        return $output;

    }


    public function rotate($angle) {
        $this->wayPoint->rotate($angle);
    }

}

class WayPoint
{
    var $position;
    var $ship;

    function __construct($e, $n)
    {
        $this->position = new Position($e, $n);
    }

    public function moveDirection($direction, $amount)
    {
        $this->position->moveDirection($direction, $amount);
    }

    public function rotate($angle)
    {
// We have to normalise angle a bit
        // Assuming we alway rotate at right angles
        $angleIndex = $angle / 90;

        while ($angleIndex < 0) {
            $angleIndex += 4;
        }
        while ($angleIndex > 3) {
            $angleIndex -= 4;
        }
        if ($angleIndex === 0) {
            return;
        }
        $newEast = $this->position->east;
        $newNorth = $this->position->north;
        switch ($angleIndex) {
            case 0:
                break;
            case 1:
                $newEast = $this->position->north;
                $newNorth = -1 * $this->position->east;
                break;
            case 2:
                $newEast = -1 * $this->position->east;
                $newNorth = -1 * $this->position->north;
                break;
            case 3:
                $newEast = -1 * $this->position->north;
                $newNorth = $this->position->east;
                break;
            default:
                throw new Exception('Bad angle: ' . $angle);
        }
        $this->position->north = $newNorth;
        $this->position->east = $newEast;

    }
}

class Position {
    var $east;
    var $north;
    function __construct($east,$north)
    {
        $this->east = $east;
        $this->north = $north;
    }

    function moveToPosition($position,$amount) {
        $this->east += $position->east * $amount;
        $this->north += $position->north * $amount;
    }

    function moveDirection($direction,$amount) {
        switch ($direction) {
            case 'N':
                $this->north += $amount;
                break;
            case 'E':
                $this->east += $amount;
                break;
            case 'S':
                $this->north -= $amount;
                break;
            case 'W':
                $this->east -= $amount;
                break;
            default:
                throw new Exception('Unknown direction',$direction);
                break;
        }
    }

    public function output() {
        $outPut = '';
        $outPut .= ($this->east >= 0) ? 'east' : 'west';
        $outPut .= ' ' . abs($this->east) . ' ';
        $outPut .= ($this->north >= 0) ? 'north' : 'south';
        $outPut .= ' ' . abs($this->north) . ' ';
        return $outPut;
    }

    public function sum() {
        return abs($this->east) + abs($this->north);
    }

}

class Instruction {
    var $letter;
    var $number;
    var $ship;

    public function __construct($instruction,$ship)
    {
        $this->letter = substr($instruction,0,1);
        $this->number = substr($instruction,1);
        $this->ship = $ship;
    }

    public function go() {
        switch ($this->letter) {
            case 'F':
                $this->ship->move($this->number);
                break;
            case 'N':
            case 'E':
            case 'S':
            case 'W':
                $this->ship->moveDirection($this->letter,$this->number);
                break;
            case 'L':
                $this->ship->rotate(-1*$this->number);
                break;
            case 'R':
                $this->ship->rotate($this->number);
                break;
        }
    }
}
