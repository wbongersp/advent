<?php

$data = file_get_contents('day6.txt');

$entries = preg_split('/\n[\s]*\n/',$data);
$total = 0;
foreach ($entries AS $group) {
    preg_match_all('/[a-z]+/',$group,$matches);
    $answers = [];
    foreach ($matches[0] AS $person) {
        foreach (str_split($person) AS $letter) {
            $answers[$letter] = $letter;
        }
    }
    $total += count($answers);
}
echo 'A1: ' . $total . PHP_EOL;

$total = 0;
foreach ($entries AS $group) {
    preg_match_all('/[a-z]+/',$group,$matches);
    $answers = [];
    foreach ($matches[0] AS $person) {
        $person_answers = str_split($person);
        foreach ($person_answers AS $letter) {
            $answers[$letter] = empty($answers[$letter])? 1 : $answers[$letter]+1;
        }
    }
    $personsInGroupCount = count($matches[0]);
    $total += count(array_filter($answers,function($answerCount) use ($personsInGroupCount) {
        return $answerCount === $personsInGroupCount;
    }));
}
echo 'A2: ' . $total . PHP_EOL;
