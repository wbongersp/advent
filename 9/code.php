<?php

$data = file_get_contents('input.txt');

$testData = '35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576';

$lines = explode("\n",$data);

$chunkSize = 25;

for($i = 0; $i < count($lines)-$chunkSize-1; $i++) {
    $preAmble = array_slice($lines,$i,$chunkSize);
    $next = $lines[$i+$chunkSize];

//    echo $i . ' ' . implode(',',$preAmble) . ' - ' . $next . PHP_EOL;

    $valid = FALSE;
    foreach ($preAmble as $number) {
        if($number*2 === $next) {
            continue;
        }
        if(in_array($next-$number,$preAmble)) {
            $valid = TRUE;
//            echo 'Valid' . $next . ' = ' . $number .' + ' . ($next-$number) . PHP_EOL;
            break;
        }
    }
    if(!$valid) {
        echo 'INVALID: ' . $next . PHP_EOL;

        // find the contiguous set
        $set = findContiguousSet($next,$lines);
        print_r($set);
        echo 'A2: ' . (min($set) + max($set));

        break;
    }
}

function findContiguousSet($number,$lines) {
    for($offset = 0; $offset < count($lines)-1; $offset++) {
        for($chunckSize = 2; count($lines) - $offset; $chunckSize++) {
            $sum = array_sum(array_slice($lines,$offset,$chunckSize));
            if($sum > $number) {
                break;
            }
            if ($sum == $number) {
                return array_slice($lines,$offset,$chunckSize);
            }
        }
    }
}






