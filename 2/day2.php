<?php


$data = preg_split("/\n/",file_get_contents('day2.txt'));

echo '1:' . PHP_EOL;
$correct = 0;
foreach ($data AS $line) {
    if(preg_match('/([0-9]+)-([0-9]+) (.*?): (.*?)$/',$line,$matches)) {
        $count = strlen(preg_replace('/[^' . $matches[3] . ']/','',$matches[4]));
        if($count >= $matches[1] && $count <= $matches[2]) {
            $correct++;
        }
    }
}
echo 'Correct: ' . $correct . PHP_EOL;


echo '2:' . PHP_EOL;
$correct = 0;
foreach ($data AS $line) {
    if(preg_match('/([0-9]+)-([0-9]+) (.*?): (.*?)$/',$line,$matches)) {
        $a = (substr($matches[4],$matches[1]-1,1) === $matches[3]);
        $b = (substr($matches[4],$matches[2]-1,1) === $matches[3]);

        if(($a && !$b) || ($b && !$a)) {
            $correct++;
        }
    }
}

echo 'Correct: ' . $correct . PHP_EOL;