<?php

$data = file_get_contents('input.txt');
$code = explode("\n", $data);

$testData = 'nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6';

class Program {
    var $acc = 0;
    var $instructions = [];
    var $position = 0;
    var $end = FALSE;
    var $loop = FALSE;
    var $passedLines = [];
    var $switched = null;

    function __construct($data)
    {
        preg_match_all('/([a-z]+) ([\-\+][0-9]+)/',$data,$matches);
        foreach ($matches[1] AS $key=>$command) {
            $this->instructions[] = [
              $command, (int) $matches[2][$key]
            ];
        }
    }

    function reset() {
        $this->position = 0;
        $this->end = FALSE;
        $this->loop = FALSE;
        $this->switched = null;
        $this->acc = 0;
        $this->passedLines = [];
    }

    function run() {
        echo '<<<<' . PHP_EOL;
        while(!$this->end && !$this->loop) {
            $this->runCommand();
        }
        echo '>>>>' . PHP_EOL.PHP_EOL;
    }

    function runCommand() {
        $info = $this->instructions[$this->position];
        $this->passedLines[] = $this->position;

        echo 'Position ' . $this->position . PHP_EOL;
        $command = $info[0];
        if($this->switched === $this->position) {
            if($info[0] === 'nop') {
                $command = 'jmp';
            }
            if($info[0] === 'jmp') {
                $command = 'nop';
            }
        }

        $jump = 1;
        switch ($command) {
            case 'nop':
                echo 'No op' . PHP_EOL;
                break;
            case 'acc':
                echo 'Acc +' . $info[1] . ': ' . $this->acc . ' -> ' . ($this->acc + $info[1]) . PHP_EOL;
                $this->acc += $info[1];
                break;
            case 'jmp':
                echo 'Jump +' . $info[1] . ': ' . $this->position . ' -> ' . ($this->position + $info[1]) . PHP_EOL;
                $jump = $info[1];
                break;
            default:
                echo 'Unknown instruction: ' . $info[0] . PHP_EOL;
                $this->end = TRUE;
                break;
        }
        $this->position += $jump;


        if(in_array($this->position,$this->passedLines)) {
            echo 'Loop detected ' . $this->position . PHP_EOL;
            $this->loop = TRUE;
        }
        if(empty($this->instructions[$this->position])) {
            echo 'Instruction line not found: ' . $this->position . PHP_EOL;
            $this->end = TRUE;
        }
    }
}

//$prog = new Program($data);
//$prog->run();
//echo 'A1 ' . $prog->acc;

$prog = new Program($data);
for($i = 0; $i <= count($prog->instructions); $i++) {
    echo $i;
    $prog->reset();
    $prog->switched = $i;
    $prog->run();
    if($prog->end) {
        echo 'Error on line ' . $i . PHP_EOL;
        break;
    }
}
echo 'A2 ' . $prog->acc . PHP_EOL;




