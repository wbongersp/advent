<?php

$data = file_get_contents('input.txt');

$testData = '939
7,13,x,x,59,x,31,19';

$realData = '1004345
41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,379,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,557,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19';


// A1
$lines = explode("\n",$realData);
$departTime = $lines[0];
$busses = explode(",",$lines[1]);
echo 'Depart time: ' . $departTime . PHP_EOL;
$firstBus = null;
$waitTime = null;

foreach ($busses as $bus) {
    if($bus === 'x') {
        continue;
    }
    $busWaitTime = -1*($departTime % $bus) + $bus;
    if($waitTime === null || $busWaitTime < $waitTime) {
        $waitTime = $busWaitTime;
        $firstBus = $bus;
    }
}

echo 'A1 ' . $firstBus . ' - ' . $waitTime . ' ' . $waitTime*$firstBus . PHP_EOL;

$busses = array_filter($busses,function($bus) { return $bus !== 'x'; });

// 17,x,13,19 = t=3417
//$busses = [
//    0 => 17,
//    2 => 13,
//    3 => 19,
//];

// 17,x,13,19 = t=3417
//$busses = [
//    0 => 2,
//    1 => 3,
////    3 => 19,
//];

$virtualBus = null;
foreach ($busses as $index=>$number) {
    $bus = new Bus($number,$index);
    if($virtualBus) {
        $virtualBus = $bus->merge($virtualBus);

    } else {
        $virtualBus = $bus;
    }
}
print_r($virtualBus);
echo 'A2 ' . $virtualBus->number - $virtualBus->index . PHP_EOL;

// We need to create vrtual busses
// Say we have a very simple ruleset
// Bus 2 (index:0), 3 (index:2), 5 (index: 3)
// For now ignoring the index, if we try to find the point where they will leave at the same time
// The answer is 0,30,60
// We can arrive at this by first combinig the first two
// Bus 2 and 3 leave at the same time every 6 minutes, so we have a virtual bus 6
// Bus 6 and 5 leave at the same time every 30 minutes
// More easily, the complete virtual bus is all numbers multiplied 2 * 3 * 5 = 30

// We can do the same with the indexes
// The valid timestamps for bus 2 and 3 is 4,10,16
// So the virtual bus has number 6 and index 4.
// If we try this for bus 3 and 5, we know the number will be 15
// Bus 3 is valid at 1,4,7,10
// Bus 5 is valid at 2,7,12
// So virtual bus 15 will be valid at 7,22,37,...

class Bus {
    var $number;
    var $index;
    function __construct($number,$index)
    {
        $this->number = $number;
        $this->index = $index;
    }

    function merge(Bus $otherBus) {

        $virtualNumber = $this->number * $otherBus->number;

        // We're assuming the otherBus is always the previous/virtual bus, so it will have the biggest number
        $t = $otherBus->firstValidTime();

        // walk other bus valid times till this is valid too
        while(!$this->validTime($t)) {
            $t = $otherBus->incrementTime($t);
        }
        return new Bus($virtualNumber,$virtualNumber-$t);
    }

    function validTime($t) {
        return (($t+$this->index) % $this->number === 0);
    }

    function firstValidTime() {
        return $this->number - $this->index;
    }

    function incrementTime($time) {
        return $time += $this->number;
    }
}