<?php
$example1 = '0,3,6';
$example1 = '3,1,2';
$a1 = '1,0,16,5,17,4';

$game = new Game($a1);

$game->play(30000000);

// A1
class Game {
    var $numbers = [];
    var $round = 1;
    var $start = [];

    function __construct($data)
    {
        $this->start = explode(',',$data);
    }

    function play($end) {
        $saveTurnNumber = null;
        while($this->round <= $end) {
            $number = 0;

            if($saveTurnNumber) {
                $number = $saveTurnNumber;
            }
            elseif(isset($this->start[$this->round-1])) {
                $number = $this->start[$this->round-1];
            }
            if($this->round < 10 || round($this->round/10000) === $this->round/10000 || $this->round > 30000000 - 10) {
                echo $this->round . ' Say: ' . $number . PHP_EOL;
            }
            $saveTurnNumber = null;

            if(isset($this->numbers[$number])) {
//                echo $number . ' was said before in turn ' . $this->numbers[$number] . ' it is now turn ' . $this->round . PHP_EOL;
                $saveTurnNumber = $this->round - $this->numbers[$number];
            }

            $this->numbers[$number] = $this->round;

            $this->round++;
        }
    }
}