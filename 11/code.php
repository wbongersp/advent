<?php

$data = file_get_contents('input.txt');

$testData = 'L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL';

simulate($testData);
simulate($data);

simulate($testData,TRUE);
simulate($data,TRUE);


function countMapChars($map,$char) {
    $count = 0;
    foreach ($map as $row) {
        $count += count(array_filter($row, function($seat) use($char) { return $seat===$char; }));
    }
    return $count;
}

function simulate($data, $farSee = FALSE) {

    $map = loadMap(explode("\n",$data));
    $change_count = 1;

    $steps = 0;
    while($change_count > 0) {

        $change_count = runStep($map,$farSee);
        $steps++;


        if($steps > 1000) {
            break;
        }
    }

    echo 'Done in: ' . $steps . PHP_EOL;

    echo 'Occupied seats: ' . countMapChars($map,'#') , PHP_EOL;
}

function loadMap($map) {
    $items = [];
    foreach ($map as $y=>$val) {
        for($x=0;$x<strlen($val);$x++) {
            $char = substr($val,$x,1);
            $items[$y][$x] = $char;
        }
    }
    return $items;
}

function runStep(&$map,$farSee = FALSE) {
    $oldMap = $map;

    $change_count = 0;

    foreach ($oldMap as $y=>$row) {
        foreach ($row as $x=>$char) {
            if($char === 'L' && enterSeat($x,$y,$oldMap,$farSee)) {
                $map[$y][$x] = '#';
                $change_count++;
            } else if($char === '#' && leaveSeat($x,$y,$oldMap,$farSee)) {
                $map[$y][$x] = 'L';
                $change_count++;
            }
        }
    }
    return $change_count;
}

function printMap($map) {
    for($i=0; $i < count($map[0]) + 2;$i++) {
        echo '_';
    }
    echo PHP_EOL;
    foreach ($map as $y=>$row) {
        echo '|';
        foreach ($row as $x=>$char) {
            echo $char;
        }
        echo '|' . PHP_EOL;
    }
    for($i=0; $i < count($map[0]);$i++) {
        echo '-';
    }
    echo PHP_EOL;
}

function getMapChar($x,$y,$dirX,$dirY,$map,$farSee) {
    if($dirX === 0 && $dirY === 0) {
        return FALSE;
    }
    $i = 1;
    while(isset($map[$y+$i*$dirY][$x+$i*$dirX])) {
        $char = $map[$y+$i*$dirY][$x+$i*$dirX];
        if($farSee && $char === '.') {
            $i++;
            continue;
        }
        return $char;
    }
    return FALSE;
}

function getAdjecentSeatOccupiedCount($x,$y,$map,$farSee) {
    $count = 0;

    // Look around
    $directions = [-1,0,1];
    foreach ($directions as $yDir) {
        foreach ($directions as $xDir) {
            $char = getMapChar($x,$y,$xDir,$yDir,$map,$farSee);
            if($char === '#') {
                $count++;
            }
        }
    }

    return $count;
}

function getAdjecentFarSeatOccupiedCount($x,$y,$map) {
    $count = 0;
    $directions = [-1,0,1];
    foreach ($directions as $yDir) {
        foreach ($directions as $xDir) {
            if($xDir === 0 && $yDir === 0) {
                continue;
            }
            $foundSeat = null;
            $i = 0;
            while ($foundSeat === null) {
                if(!isset($map[$y+$i*$yDir][$x+$i*$xDir])) {
                    break;
                }
                $char = $map[$y+$i*$yDir][$x+$i*$xDir];
                if($char !== '.') {
                    $foundSeat = $char;
                }
            }
            if($foundSeat === '#') {
                $count++;
            }
        }
    }
    return $count;
}

function enterSeat($x,$y,$map,$farSee) {
    return (getAdjecentSeatOccupiedCount($x,$y,$map,$farSee) === 0);
}

function leaveSeat($x,$y,$map,$farSee) {
    $limit = ($farSee) ? 5 : 4;

    return (getAdjecentSeatOccupiedCount($x,$y,$map,$farSee) >= $limit);
}

