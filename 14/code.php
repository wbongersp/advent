<?php

$data = file_get_contents('input.txt');

$testData = 'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0';

$testData2 = 'mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1';


// A1
$lines = explode("\n",$data);
$memory = new Memory();
$memory->run($testData);
echo 'Mem total: ' . $memory->memTotal() . PHP_EOL;

// A2
$memory = new Memory();
$memory->version = 2;
$memory->run($data);
echo 'Mem total: ' . $memory->memTotal() . PHP_EOL;


class Memory {
    var $mask;
    var $memory = [];
    var $version = 1;

    function setMask($mask)
    {
        if(!$this->version === 1) {
            $this->mask = preg_replace('/^[X]+/','',$mask);
        } else {
            $this->mask = preg_replace('/^[0]+/','',$mask);
        }
    }

    function run($data) {
        $lines = explode("\n",$data);
        foreach ($lines as $line) {
            if(preg_match('/mask.*?([X10]+)$/',$line,$match)) {
                $this->setMask($match[1]);
            } elseif (preg_match('/mem\[(\d+)\] = (\d+)/',$line,$match)) {
                $this->writeMem($match[1],$match[2]);
            }
        }
    }


    function writeMem($address,$number) {
        if($this->version === 1) {
            $this->memory[$address] = $this->applyMask($number);
        } else {
            $addresses = $this->applyMemoryMask($address);
            foreach ($addresses as $address) {
                $this->memory[$address] = $number;
            }
        }
    }

    function applyMask($number) {
        $binaryNumber = decbin($number);
        $new = '';

        // Prepend binary nunmber
        while(strlen($binaryNumber) < strlen($this->mask)) {
            $binaryNumber = '0' . $binaryNumber;
        }

        $mask = $this->mask;
        while(strlen($mask) < strlen($binaryNumber)) {
            $mask = 'X' . $mask;
        }

        for($i=0; $i < strlen($mask);$i++) {
            $maskChar = substr($mask,$i,1);
            $binaryChar = substr($binaryNumber,$i,1);

            switch ($maskChar) {
                case 'X':
                    $new .= $binaryChar;
                    break;
                case '1':
                case '0':
                    $new .= $maskChar;
                    break;
                default:
                    throw new Exception('Unknown mask char');

            }
        }
        return bindec($new);
    }

    function prepend($string,$compareString,$prefix) {
        while(strlen($string) < strlen($compareString)) {
            $string = '0' . $string;
        }
        return $string;
    }


    function applyMemoryMask($number) {

        $binaryNumber = decbin($number);

        $variations = [];

        // Prepend both strings
        $binaryNumber = $this->prepend($binaryNumber,$this->mask,'0');
        $mask = $this->prepend($this->mask,$binaryNumber,'0');

        $base = '';
        for($i=0; $i < strlen($mask);$i++) {
            $maskChar = substr($mask,$i,1);
            $binaryChar = substr($binaryNumber,$i,1);

            switch ($maskChar) {
                case '0':
                    $base .= $binaryChar;
                    break;
                case '1':
                    $base .= '1';
                    break;
                case 'X':
                    // Add the lowest value to the base but save the higher value in the variations, we can add it later.
                    $base .= '0';
                    $variations[] = $this->prepend('1',$base,'0');
                    break;
                default:
                    throw new Exception('Unknown mask char');

            }
        }

        $results = [bindec($base)];

        // Now it's time to add the variations
        foreach ($variations as $variation) {
            while(strlen($base) > strlen($variation)) {
                $variation .= '0';
            }
            foreach ($results as $result) {
                $results[] = $result + bindec($variation);
            }
        }
        return $results;
    }

    function memTotal() {
        return array_sum($this->memory);
    }

}