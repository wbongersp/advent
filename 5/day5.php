<?php
$lines = preg_split("/\n/",file_get_contents('day5.txt'));

function getBinary($code,$letter,$start,$end) {
    $value = 0;
    for($i=$start; $i<$end;$i++) {
        if(substr($code,$i,1) === $letter) {
            $value += pow(2,$end-1-$i);
        }
    }
    return $value;
}

function getRow($code) {
    return getBinary($code,'B',0,7);
}

function getCol($code) {
    return getBinary($code, 'R',7,10);
}
function seatID($code) {
    return getRow($code) * 8 + getCol($code);
}

// Part 1
$max = 0;
foreach ($lines as $code) {
    $max = max(seatID($code),$max);
}
echo 'Part 1:' . $max . PHP_EOL;

// Part 2
$seats = [];
foreach ($lines as $code) {
    $seats[seatID($code)] = $code;
}
ksort($seats);
foreach ($seats AS $id=>$code) {
    if(!isset($seats[$id + 1]) && isset($seats[$id + 2])) {
        echo 'Part 2: ' . ($id+1)    . PHP_EOL;
    }
}

